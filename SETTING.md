![bams_swaggerui](https://user-images.githubusercontent.com/74520820/102179995-7bc9ee00-3eeb-11eb-9e14-a4777019204b.png)

# Settings

## Sample Project
```
.sample
|-- myapi_A
|   |-- README.md
|   |-- bamscatalog.json
|   |-- myapi01
|   `-- myapi02
|-- myapi_B
|   |-- README.md
|   |-- bamscatalog.json
|   |-- myapi03
|   `-- myapi04
`-- bamscatalogenv.js

```
## bamscatalogenv.js
BAMsCatalogの設定を記述するファイルです。

 - VSCode拡張機能の場合 ... ワークスペースのルートフォルダに設置します。

 - GithubPagesをForkした場合 ... プロジェクトのルートフォルダに設置します。

|  設定項目  |  説明  |
| ---- | ---- |
|  API_REPOSITORY_NAME  |  API読み込み対象のフォルダまでの相対パスを入力します  |
|  DEFAULT_API_DOCUMENT_VIEW  |  redoc または swagger を設定します  |
|  REDOC_OPTIONS  |  [Redocのオプション](https://redoc.ly/docs/cli/configuration/reference-docs/#feature-options)をjson形式で入力します  |
|||

```
# bamscatalogenv.js sample
API_REPOSITORY_NAME=myapi_A
DEFAULT_API_DOCUMENT_VIEW=redoc
REDOC_OPTIONS={"theme":{"colors":{"primary":{"main":"#dd5522"}}}}
```

## bamscatalog.json
BAMs Catalogに表示するAPIの情報と、APIフォルダまでの相対パスを記述するファイルです。

```
# bamscatalog.json sample
[
  {
    "name":"観光地API", # APIの名称
    "category":"観光", # APIのカテゴリ
    "api":"./myapi01/spots/openapi.json", # openapiの定義ファイルまでの相対パス
    "description": "観光地情報を登録・更新・削除し、リストや詳細情報を取得するAPIです。各種データは会津若松プラスから取得しました。", # APIの説明
    "images": "./myapi01/spots/spots.jpg" # APIの画像までの相対パス
  },
  ...
]
```
上記の設定を行うことで、以下のようなカードが表示されます。

<img width="916" alt="bams_catalog_grid_view" src="https://user-images.githubusercontent.com/74520820/105802181-feab9180-5fdd-11eb-8860-538dc75ca0e9.png">